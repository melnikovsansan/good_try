import DS from 'ember-data';

var User = DS.Model.extend({
  firstName: DS.attr('string'),
  lastName: DS.attr('string'),
  age: DS.attr('number')
});

User.reopenClass({
  FIXTURES: []
});

export default User;
