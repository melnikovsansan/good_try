import DS from 'ember-data';

var Panel = DS.Model.extend({
  name: DS.attr('string')
});

Panel.reopenClass({
  FIXTURES: []
});

export default Panel;
