import Ember from "ember";
import config from "good-try/config/environment";

var Router = Ember.Router.extend({
  location: config.locationType
});

export default Router.map(function() {
  this.route('login');
  this.resource("users", function() {
    this.route("new");

    this.route("edit", {
      path: ":user_id/edit"
    });

    this.route("show", {
      path: ":user_id"
    });
  });
  this.resource("panels", {path: '/'}, function() {
    this.route("show", {
      path: ":name"
    });
  });
});
