import Ember from 'ember';

import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin';

export default Ember.Route.extend(ApplicationRouteMixin, {
  setupController: function(controller,model) {
    // `controller` is the instance of ApplicationController
    controller.set('usersTabTitle', "Scaffold Users");
    controller.set('model', model);
  }
});
